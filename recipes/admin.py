from django.contrib import admin
from recipes.models import Recipe


# Register your models here.
# admin.site.register(Recipe)
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )
