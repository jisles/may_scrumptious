from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from recipes.models import Recipe


# # Create your views here.
# def show_recipe(request): #will always take in a 'request'
#     return render(request, "recipes/detail.html")
#     # return HttpResponse("This view is working")


def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)
